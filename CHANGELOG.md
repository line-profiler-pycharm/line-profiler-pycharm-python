# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2024-09-23
### Added
- Each function profile can have a different time-unit set. 
  The Pycharm Line Profiler plugin (from 1.8.0) is able to use these time-units and display timings accordingly
  This package can still be used for older versions of the Pycharm plugin

## [1.1.0] - 2021-06-14
### Changed
- Disable the line_profiler wrapper when executing in the PyCharm debugger

## [1.0.0] - 2021-02-22
### Added
- PyCharm adapter package called `line-profiler-pycharm` that should be installed to the python environments 
  with which one wants to profile code and visualize those profiles 
- A decorator that writes the `.pclprof` files which can be read by 
  the PyCharm Line Profiler plugin


[Unreleased]: https://gitlab.com/line-profiler-pycharm/line-profiler-pycharm-python/tree/dev/
[1.2.0]: https://gitlab.com/line-profiler-pycharm/line-profiler-pycharm-python/tree/v1.2.0/
[1.1.0]: https://gitlab.com/line-profiler-pycharm/line-profiler-pycharm-python/tree/v1.1.0/
[1.0.0]: https://gitlab.com/line-profiler-pycharm/line-profiler-pycharm-python/tree/v1.0.0/
